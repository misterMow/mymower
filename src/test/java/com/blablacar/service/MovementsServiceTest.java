package com.blablacar.service;

import com.blablacar.exceptions.InvalidMovementException;
import com.blablacar.model.Case;
import com.blablacar.model.Mower;
import com.blablacar.model.OrientationEnum;
import com.blablacar.model.Square;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MovementsServiceTest {

    MovementsService movementsService = new MovementsService();

    @Test
    public void shouldMoveCorrectlyAMower() throws InvalidMovementException {
        Square garden = new Square(5, 5);
        Case position = new Case(0, 0);
        Mower mow = new Mower(position, "N");
        movementsService.moveForward(mow, garden);
        Assert.assertEquals(1, mow.getPosition().getY().intValue());
    }

    @Test(expected = InvalidMovementException.class)
    public void shouldNotMoveOutsideAMower() throws InvalidMovementException {
        Square garden = new Square(5, 5);
        Case position = new Case(0, 5);
        Mower mow = new Mower(position, "N");
        movementsService.moveForward(mow, garden);
    }
}
