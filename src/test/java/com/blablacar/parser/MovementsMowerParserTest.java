package com.blablacar.parser;


import com.sun.media.sound.InvalidFormatException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class MovementsMowerParserTest {

    MovementsMowerParser movementsMowerParser = new MovementsMowerParser();

    @Test(expected = InvalidFormatException.class)
    public void shouldNotValidateMovementsInputWithWrongPattern() throws InvalidFormatException {
        String input = "5/5";
        Assert.assertFalse(movementsMowerParser.checkInput(input));
    }

    @Test
    public void shouldValidateMovementsInputWithGoodPattern() throws InvalidFormatException {
        String input = "LFFLFLFLFLRRRR";
        Assert.assertTrue(movementsMowerParser.checkInput(input));
    }

}