package com.blablacar.parser;


import com.sun.media.sound.InvalidFormatException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SquareSizeParserTest {

    SquareSizeParser squareSizeParser = new SquareSizeParser();

    @Test(expected = InvalidFormatException.class)
    public void shouldNotValidateInputWithWrongPattern() throws InvalidFormatException {
        String input = "5/5";
        Assert.assertFalse(squareSizeParser.checkInput(input));
    }

    @Test
    public void shouldValidateInputWithGoodPattern() throws InvalidFormatException {
        String input = "5 5";
        Assert.assertTrue(squareSizeParser.checkInput(input));
    }

    @Test
    public void shouldParseInputWithGoodPattern() throws Exception {
        Integer[] expectedGardenSize = {5, 5};
        String input = "5 5";
        Assert.assertArrayEquals(expectedGardenSize, squareSizeParser.parseInput(input));
    }
}