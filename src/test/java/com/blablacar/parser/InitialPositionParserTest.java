package com.blablacar.parser;


import com.blablacar.model.Case;
import com.blablacar.model.Mower;
import com.sun.media.sound.InvalidFormatException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class InitialPositionParserTest {

    InitialPositionParser initialPositionParser = new InitialPositionParser();

    @Test(expected = InvalidFormatException.class)
    public void shouldRaiseAnExceptionIfInitialPositionInputWithWrongPattern() throws InvalidFormatException {
        String input = "1 2 Q A";
        initialPositionParser.checkInput(input);
    }

    @Test
    public void shouldAcceptAnInitialPositionInputWithGoodPattern() throws InvalidFormatException {
        String input = "1 2 N";
        Assert.assertTrue(initialPositionParser.checkInput(input));
    }

    @Test
    public void shouldOverrideCorrectlyMowerMethodeEquals() {
        Case mowerPosition = new Case(1,2);
        Mower mower1 = new Mower(mowerPosition, "N");
        Mower mower2 = new Mower(mowerPosition, "N");
        Assert.assertTrue(mower1.equals(mower2));
    }

    @Test
    public void twoEqualsMowerShouldHaveTheSameHashCode() {
        Case mowerPosition = new Case(1,2);
        Mower mower1 = new Mower(mowerPosition, "N");
        Mower mower2 = new Mower(mowerPosition, "N");
        Assert.assertEquals(mower1.hashCode(), mower2.hashCode());
    }

    @Test
    public void twoDifferentsMowerShouldNotHaveTheSameHashCode() {
        Case mowerPosition = new Case(1,2);
        Mower mower1 = new Mower(mowerPosition, "N");
        Mower mower2 = new Mower(mowerPosition, "S");
        Assert.assertNotEquals(mower1.hashCode(), mower2.hashCode());
    }

    @Test
    public void shouldParseCorrectlyAMowerInitialPosition() {
        Case mowerPosition = new Case(1,2);
        Mower expectedMower = new Mower(mowerPosition, "N");
        String input = "1 2 N";
        Assert.assertEquals(initialPositionParser.parseInput(input), expectedMower);
    }
}