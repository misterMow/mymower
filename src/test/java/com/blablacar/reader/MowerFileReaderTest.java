package com.blablacar.reader;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.io.FileInputStream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MowerFileReaderTest {

    MowerFileReader fileReader = MowerFileReader.getInstance();

    @Test
    public void shouldOpenFileSuccessfuly() {
        FileInputStream inputStream = fileReader.openFile("src/test/resources/mowerMovementsTest");
        Assert.assertNotNull(inputStream);
    }

    @Test
    public void shoudlReadSquareSize() {
        FileInputStream inputStream = fileReader.openFile("src/test/resources/mowerMovementsTest");
        Assert.assertNotNull(inputStream);
    }
}
