package com.blablacar;

import com.blablacar.exceptions.InvalidMovementException;
import com.blablacar.exceptions.MowerInitializationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileNotFoundException;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MowerApplicationTest {

	@Test(expected = FileNotFoundException.class)
	public void runAppWithoutFileTestShouldRaiseFileNotFoundException() throws FileNotFoundException, MowerInitializationException, InvalidMovementException {
		MowerApplication.main(new String[] {});
	}

	@Test
	public void runApp() throws FileNotFoundException, MowerInitializationException, InvalidMovementException {
		MowerApplication.main(new String[] {"mowerMovements"});
	}

}
