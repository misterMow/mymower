    package com.blablacar.service;

    import com.blablacar.exceptions.InvalidMovementException;
    import com.blablacar.model.*;

    public class MovementsService {

        public void moveOrientation(MovementEnum movement, Mower mower) {

            if (mower.getOrientation().equals(OrientationEnum.NORTH)) {
                if (movement.equals(MovementEnum.LEFT)) { mower.setOrientation(OrientationEnum.WEST); }
                else if (movement.equals(MovementEnum.RIGHT)) { mower.setOrientation(OrientationEnum.EAST); }
            }

            else if (mower.getOrientation().equals(OrientationEnum.SOUTH)) {
                if (movement.equals(MovementEnum.LEFT)) { mower.setOrientation(OrientationEnum.EAST); }
                else if (movement.equals(MovementEnum.RIGHT)) { mower.setOrientation((OrientationEnum.WEST)); }
            }

            else if (mower.getOrientation().equals(OrientationEnum.EAST)) {
                if (movement.equals(MovementEnum.LEFT)) {  mower.setOrientation(OrientationEnum.NORTH); }
                else if (movement.equals(MovementEnum.RIGHT)) { mower.setOrientation(OrientationEnum.SOUTH); }
            }

            else if (mower.getOrientation().equals(OrientationEnum.WEST)) {
                if (movement.equals(MovementEnum.LEFT)) { mower.setOrientation(OrientationEnum.SOUTH); }
                else if (movement.equals(MovementEnum.RIGHT)) { mower.setOrientation(OrientationEnum.NORTH); }
            }
        }

        public boolean moveForward(Mower mower, Square garden) throws InvalidMovementException {

            boolean isIncorrectMovement = false;
            Integer x = mower.getPosition().getX();
            Integer y = mower.getPosition().getY();

            if (mower.getOrientation().equals(OrientationEnum.NORTH)) {
                if (y + 1 <= garden.getY()) {
                    mower.setPosition((new Case(x, y + 1)));
                } else { isIncorrectMovement = true; throw new InvalidMovementException("Moving outside of the field is not allowed"); }

            } else if (mower.getOrientation().equals(OrientationEnum.SOUTH)) {
                if (y - 1 >= 0) {
                    mower.setPosition(new Case(x, y - 1));
                } else { isIncorrectMovement = true; throw new InvalidMovementException("Moving outside of the field is not allowed");}

            } else if (mower.getOrientation().equals(OrientationEnum.EAST)) {
                if (x + 1 <= garden.getX()) {
                        mower.setPosition(new Case(x + 1, y));
                } else { isIncorrectMovement = true; throw new InvalidMovementException("Moving outside of the field is not allowed");}

            } else if (mower.getOrientation().equals(OrientationEnum.WEST)) {
                if (x - 1 >= 0) {
                    mower.setPosition(new Case(x - 1, y));
                } else { isIncorrectMovement = true; throw new InvalidMovementException("Moving outside of the field is not allowed");}
            }

            return isIncorrectMovement;
        }
    }
