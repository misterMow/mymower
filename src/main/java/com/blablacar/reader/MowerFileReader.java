package com.blablacar.reader;

import com.blablacar.controller.MowerController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MowerFileReader {

    public static final Logger logger = LoggerFactory.getLogger(MowerController.class);

    private static final MowerFileReader instance = new MowerFileReader();

    private MowerFileReader() {}

    public static MowerFileReader getInstance() {
        return instance;
    }

    public List<String> process(String filename) {
        return readFile(filename);
    }


    public FileInputStream openFile(String fileName) {
        FileInputStream inputFile = null;
        try {
            File movementsFile = new File(fileName);
            inputFile = new FileInputStream(movementsFile);
        } catch (FileNotFoundException e) {
            logger.error("Fichier d'entrée introuvable");
        }
        return inputFile;
    }


    public List<String> readFile(String fileName) {

        List<String> fileLines = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(openFile(fileName)));
        try {
            while (reader.ready()) {
                String line = reader.readLine();
                logger.info("Ligne " + line);
                fileLines.add(line);
            }
            reader.close();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return fileLines;
    }






}
