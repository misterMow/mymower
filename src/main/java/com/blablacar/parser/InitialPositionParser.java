package com.blablacar.parser;


import com.blablacar.model.Case;
import com.blablacar.model.Mower;
import com.sun.media.sound.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class InitialPositionParser extends AbstractMowerParser {

    private static final Logger logger = LoggerFactory.getLogger(InitialPositionParser.class);
    private final String pattern = "^[0-9]{1,}\\s{1}[0-9]{1,}\\s[N,S,E,W]{1}$";


    public boolean checkInput(String input) throws InvalidFormatException {
        return super.checkInput(input, pattern);
    }

    @Override
    public Mower parseInput(String input) {
        Case initialPosition = null;
        Mower initialPositionMower = null;
        try {
            checkInput(input);
            String[] splittedValues = input.split("\\s");
            initialPosition = new Case(Integer.valueOf(splittedValues[0]), Integer.valueOf(splittedValues[1]));
            initialPositionMower = new Mower(initialPosition, splittedValues[2]);
            logger.info("Initial position of the mower: " +  initialPositionMower.getPosition().getX() + " " + initialPositionMower.getPosition().getY() + " " + initialPositionMower.getOrientation().getCode());
        }
        catch (InvalidFormatException exception) {
            logger.error(" Incorrect pattern for the initial position of the mower");
        }
        return initialPositionMower;
    }
}
