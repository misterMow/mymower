package com.blablacar.parser;

import com.blablacar.model.MovementEnum;
import com.sun.media.sound.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Component
public class MovementsMowerParser extends AbstractMowerParser {

    private static final Logger logger = LoggerFactory.getLogger(MovementsMowerParser.class);
    private final String pattern = "^[L,R,F]{1,}$";

    public boolean checkInput(String input) throws InvalidFormatException {
        return super.checkInput(input, pattern);
    }

    @Override
    public List<MovementEnum> parseInput(String input) {
        List<MovementEnum> movements =  new ArrayList<>();
        try {
            checkInput(input);
            char[] movementsValues = input.toCharArray();
            for (char c : movementsValues) {
                movements.add(MovementEnum.getMovementCode(String.valueOf(c)));
            }
        }
        catch (InvalidFormatException exception) {
            logger.error(" Movements on the field don't respect the pattern L R or F");
        }
        return movements;
    }
}
