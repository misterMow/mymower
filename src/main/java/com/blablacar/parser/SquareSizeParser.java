package com.blablacar.parser;


import com.sun.media.sound.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class SquareSizeParser extends AbstractMowerParser {

    private static final Logger logger = LoggerFactory.getLogger(SquareSizeParser.class);
    private final String pattern = "^[0-9]{1,}\\s{1}[0-9]{1,}$";


    public boolean checkInput(String input) throws InvalidFormatException {
       return super.checkInput(input, pattern);
    }

    @Override
    public Integer[] parseInput(String input) {
        Integer[] gardenSize = new Integer[2];
        try {
            checkInput(input);
            String[] splittedValues = input.split("\\s");
            gardenSize[0] = Integer.parseInt(splittedValues[0]);
            gardenSize[1] = Integer.parseInt(splittedValues[1]);
        }
        catch (InvalidFormatException exception) {
            logger.error(" Le format de la taille du terrain est incorrect ");
        }
        return gardenSize;
    }


}
