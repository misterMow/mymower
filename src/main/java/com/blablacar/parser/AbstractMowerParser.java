package com.blablacar.parser;

import com.sun.media.sound.InvalidFormatException;
import org.springframework.util.StringUtils;

public abstract class AbstractMowerParser<T> {

    /**
     *  Validate the format of the line depending the associated action
     * @param input the line to control
     * @param pattern the pattern the line has to respect
     * @return
     * @throws InvalidFormatException
     */
    protected boolean checkInput(String input, String pattern) throws InvalidFormatException {
        boolean isValid = !StringUtils.isEmpty(input) && input.matches(pattern);
        if (!isValid) {
            throw new InvalidFormatException();
        }
        return isValid;
    }


    /**
     *  If the pattern is correct, this method will parse the line and return value under a type the controller can handle
     * @param input the line to parse
     * @return mower's datas for the controller
     */
    protected abstract T parseInput(String input);

}
