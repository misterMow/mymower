package com.blablacar.exceptions;

public class MowerInitializationException extends Exception {

    public MowerInitializationException(String message) {
        super(message);
    }
}
