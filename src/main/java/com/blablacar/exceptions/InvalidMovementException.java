package com.blablacar.exceptions;

public class InvalidMovementException extends Exception {

    public InvalidMovementException(String exception){
        super(exception);
    }

}
