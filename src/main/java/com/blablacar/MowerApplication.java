package com.blablacar;

import com.blablacar.controller.MowerController;
import com.blablacar.exceptions.InvalidMovementException;
import com.blablacar.exceptions.MowerInitializationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;

import java.io.FileNotFoundException;


@SpringBootApplication
@Controller
public class MowerApplication {

	private static MowerController controller = new MowerController();

	public static final Logger logger = LoggerFactory.getLogger(MowerApplication.class);


	/**
	 *  Entry point of the application
	 * @param args movements file name
	 * @throws FileNotFoundException
	 */

	public static void main(String[] args) throws FileNotFoundException, MowerInitializationException, InvalidMovementException {

		if (args != null && args.length > 0) {
			controller.process(args[0]);
		}
		else {
			logger.error("Movements file not found");
			throw new FileNotFoundException();
		}
	}
}
