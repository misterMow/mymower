package com.blablacar.controller;

import com.blablacar.exceptions.InvalidMovementException;
import com.blablacar.exceptions.MowerInitializationException;
import com.blablacar.model.MovementEnum;
import com.blablacar.model.Mower;
import com.blablacar.model.Square;
import com.blablacar.parser.InitialPositionParser;
import com.blablacar.parser.MovementsMowerParser;
import com.blablacar.parser.SquareSizeParser;
import com.blablacar.reader.MowerFileReader;
import com.blablacar.service.MovementsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import java.util.Iterator;
import java.util.List;


@Controller
public class MowerController {

    private static final Logger logger = LoggerFactory.getLogger(MowerController.class);

    MowerFileReader  fileReader = MowerFileReader.getInstance();


    /**
     *  Global cntroller. Call every differents layers of the application
     * @param fileName the name of the movement file
     */
    public void process(String fileName) throws MowerInitializationException, InvalidMovementException {

        List<String> fileInput = fileReader.process(fileName);
        Iterator<String> currentLine = fileInput.iterator();
        int mowercpt = 1;
        // Creation of the field
        SquareSizeParser mowerParser = new SquareSizeParser();
        Integer[] size = mowerParser.parseInput(currentLine.next());
        Square garden = new Square(size[0], size[1]);

        // Parsing of the file
        while (currentLine.hasNext()) {

            // Initial position of the mower
            logger.info("-----> Initialization of the mower " + mowercpt);
            InitialPositionParser initialPositionParser = new InitialPositionParser();
            Mower currentMower = initialPositionParser.parseInput(currentLine.next());
            if (currentMower == null) {
                throw new MowerInitializationException("Error during the initialization of the mower");
            }

            // Mower's movements parsing
            MovementsMowerParser movementsParse = new MovementsMowerParser();
            List<MovementEnum> mowerMovements = movementsParse.parseInput(currentLine.next());

            //Moving the mower on the field
            move(currentMower, mowerMovements, garden);

            logger.info("-----> Last Position of the mower " + mowercpt + ": " + currentMower.getPosition().getX() + ", " + currentMower.getPosition().getY() + " " + currentMower.getOrientation());
            mowercpt++;
        }

        logger.info("************************ END ************************ ");
    }

    /**
     *  Method called by the controller to move all the mowers on the field
     * @param currentMower the current mower to move
     * @param movements the list of the movements for the mower
     * @param garden the field
     */
    public void move(Mower currentMower, List<MovementEnum> movements, Square garden) throws InvalidMovementException {
        MovementsService movementsService = new MovementsService();
        boolean errorDuringMovement = false;

        // Moving the mower
        for (MovementEnum currentMove : movements) {
            if (!errorDuringMovement) {
                if (currentMove.equals(MovementEnum.LEFT) || currentMove.equals(MovementEnum.RIGHT)) {
                    movementsService.moveOrientation(currentMove, currentMower);
                } else if (currentMove.equals(MovementEnum.FORWARD)) {
                    errorDuringMovement = movementsService.moveForward(currentMower, garden);

                }
            }
        }
    }
}
