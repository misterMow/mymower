package com.blablacar.model;

    public enum OrientationEnum {

            NORTH("N"), SOUTH("S"), EAST("E"), WEST("W");

            private String orientationCode;

            OrientationEnum(String orientation) {
                this.orientationCode = orientation;
            }

            public String getCode() {
                return this.orientationCode;
            }
    }

