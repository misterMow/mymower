package com.blablacar.model;


public class Case {

    private Integer x;
    private Integer y;


    public Case(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;

        if (obj != null) {
            final Case caseToCompare = (Case) obj;
            if (caseToCompare.getX() == this.x && caseToCompare.getY() == this.y ) {
                result = true;
            }
        }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.x.hashCode();
        hash = 53 * hash + this.y.hashCode();

        return hash;
    }
}
