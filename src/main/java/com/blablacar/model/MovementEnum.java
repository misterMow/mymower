package com.blablacar.model;

    public enum MovementEnum {

        LEFT("L"), RIGHT("R"), FORWARD("F");

        private String movementCode;

        MovementEnum(String movement) {
            this.movementCode = movement;
        }

        public String getCode() {
            return this.movementCode;
        }

        public static MovementEnum getMovementCode(String code) {
            MovementEnum result = null;
            for (MovementEnum movementEnum : MovementEnum.values()) {
                if ( movementEnum.getCode().equals(code) ) {
                    result = movementEnum;
                }
            }
            return result;
        }
    }

