package com.blablacar.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Square {

    public static final Logger logger = LoggerFactory.getLogger(Square.class);

    private final int x;
    private final int y;

    public Square(int x, int y) {
        this.x = x;
        this.y = y;
        logger.info("Creating square of size (" + x +"," + y + ")");
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
