package com.blablacar.model;

public class Mower {

        private Case position;
        private OrientationEnum orientation;

        public Mower (Case position, String orientation) {
                this.position = position;
                for (OrientationEnum o : OrientationEnum.values()) {
                        if (o.getCode().equals(orientation)) {
                                this.orientation = o;
                        }
                }
        }

        public Case getPosition() {
                return position;
        }

        public void setPosition(Case position) {
                this.position = position;
        }

        public OrientationEnum getOrientation() {
                return orientation;
        }

        public void setOrientation(OrientationEnum orientation) {
                this.orientation = orientation;
        }



        @Override
        public boolean equals(Object obj) {
                boolean result = false;

                if (obj != null) {
                        final Mower mowerToCompare = (Mower) obj;
                        if (this.getOrientation().getCode().equals(mowerToCompare.getOrientation().getCode()) && this.getPosition().equals(mowerToCompare.getPosition())) {
                                result = true;
                        }
                }
                return result;
        }

        @Override
        public int hashCode() {
                int hash = 3;
                hash = 53 * hash + this.getOrientation().getCode().hashCode();
                hash = 53 * hash + this.getPosition().hashCode();
                return hash;
        }
}
